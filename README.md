### Introduction

A scrapy project to crawl news from reuters.com, time.com, nytimes.com, 
theguardian.com and cnbc.com. The extracted documents are stored in MongoDB.

### Instruction

1. Rename settings_template.py to settings.py
2. Fill MongoDB information in MONGO_URI in settings.py
3. To crawl, run `python run_spider.py` in project root directory

### Script Arguments

1. `--spider_names`: The name of spider to be run. The list of spiders can be
viewed by running `scrapy list` in project root directory. If this argument is 
not provided, then scrapy will crawl all spiders.
2. `--num_news`: The number of news to be crawled for each spider. If this 
option is not provided, then the script will crawl all news without limit.
3. `--log_every`: How often the script needs to print to the screen, informing
how many documents have been inserted to MongoDB.

### Examples
* Crawl 100 news of Reuters, log to the screen every 20 documents.

`python run_spider.py --spider_names nytimes --num_news 100 --log_every 20`

* Crawl 200 news each of Reuters and New York Times, log to the screen every 50 
documents.

`python run_spider.py --spider_names reuters nytimes --num_news 200 --log_every 50`

* Crawl 150 news of all news websites, log to the screen every 30 documents.

`python run_spider.py --num_news 150 --log_every 50`


### MongoDB Setup
1. It is recommended to set unique index {'source_name': 1, 'doc_id': 1} on
MongoDB collection

### Todo
* [x] Reuters
* [x] New York Times
* [x] The Guardian
* [x] Time
* [x] CNBC
