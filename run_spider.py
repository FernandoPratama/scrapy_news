#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor

import argparse
from scrapy.utils.misc import walk_modules
from scrapy.utils.spider import iter_spider_classes

"""
The script to crawl spiders.
Ensure to check settings.py before running this script.
In case of error, the script has to be stopped manually by pressing Ctrl-C.
"""


def get_spider_names(spider_module='scrapy_news.spiders'):
    """
    Get all spider names within a module
    :param spider_module: Location of spider module relative to the location
    where this script is run
    :return: List of spider names
    """
    all_spider_names = []
    for mod in walk_modules(spider_module):
        for spider_class in iter_spider_classes(mod):
            all_spider_names.append(spider_class.name)
    return all_spider_names


def main(spider_names, num_news, log_every):
    """
    Run crawler for each spider name, store the results in MongoDB
    Check settings.py to configure MongoDB setup
    :param spider_names: List of spider names
    :param num_news: The number of news to be crawled for each spider
    :param log_every: Log to the screen for every 'log_every' news
    :return: None
    """
    settings = get_project_settings()
    settings.set("CLOSESPIDER_ITEMCOUNT", num_news)
    settings.set("LOG_EVERY", log_every)

    runner = CrawlerRunner(settings)

    for spider_name in spider_names:
        runner.crawl(spider_name)
    d = runner.join()
    d.addBoth(lambda _: reactor.stop())
    reactor.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Python Script to Run Crawl Article News',
    )
    parser.add_argument('--spider_names',
                        action="store",
                        nargs="+",
                        dest='spider_names',
                        default=[],
                        help="Spider name")
    parser.add_argument('--num_news',
                        action="store",
                        type=int,
                        dest="num_news",
                        default=0,
                        help="Number of news to be scrapped on each website")
    parser.add_argument('--log_every',
                        action="store",
                        type=int,
                        dest="log_every",
                        default=1,
                        help="Notify how many documents inserted to MongoDB")

    parsed_args = parser.parse_args()
    # Check spider_names argument
    possible_spider_names = get_spider_names('scrapy_news.spiders')
    # Raise error if spider name is not available
    for spider_name in parsed_args.spider_names:
        if spider_name not in possible_spider_names:
            raise ValueError('%s spider is not available' % spider_name)
    # If spider_names argument is empty, then crawl all spiders
    if not parsed_args.spider_names:
        parsed_args.spider_names = possible_spider_names

    # Sanity check on num_news and log_every arguments
    if not parsed_args.num_news >= 0:
        raise ValueError("num_news should be greater or equal than 0")
    if not parsed_args.log_every > 0:
        raise ValueError("log_every should be greater than 0")

    main(parsed_args.spider_names,
         parsed_args.num_news,
         parsed_args.log_every)
