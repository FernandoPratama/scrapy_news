| Name                | Type           | Necessary | Description                                                                                     |
|---------------------|----------------|-----------|-------------------------------------------------------------------------------------------------|
| doc_id              | String         | Yes       | The unique article id, each website may have different format for doc_id convert them to string |
| title               | String         | Yes       | Article title                                                                                   |
| text                | String         | Yes       | Article content                                                                                 |
| description         | String         | Yes       | Article description, typically not shown in website                                             |
| published_timestamp | Long           | Yes       | Timestamp when the article is published                                                         |
| published_date      | Datetime       | Yes       | Date when the article is published in UTC timezone                                              |
| crawled_timestamp   | Long           | Yes       | Timestamp when the article is crawled                                                           |
| crawled_date        | Datetime       | Yes       | Date when the article is crawled in UTC timezone                                                |
| url                 | String         | Yes       | Article url                                                                                     |
| topic               | String         | Yes       | Article topic                                                                                   |
| subtopic            | String         | No        | Article subtopic                                                                                |
| tags                | List of String | Yes       | Article tag                                                                                     |
| language            | String         | Yes       | Article language                                                                                |
| source_type         | String         | Yes       | Whether the article is news, blog post, or social media post                                    |
| source_name         | String         | Yes       | The publisher name                                                                              |
| domain              | String         | Yes       | The publisher url                                                                               |
|image | String | No | The biggest image in the article, store it in url format
| source_country | String | No | Publisher's country of origin, Use 'Alpha-3 Code' from this [website](https://en.wikipedia.org/wiki/ISO_3166-1)

