# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class NewsItem(Item):
    doc_id = Field()
    title = Field()
    text = Field()
    description = Field()
    published_timestamp = Field()
    published_date = Field()
    crawled_timestamp = Field()
    crawled_date = Field()
    url = Field()
    tags = Field()
    language = Field()
    source_type = Field()
    source_name = Field()
    domain = Field()
    source_country=Field()
    image=Field()
    topic=Field()
    subtopic=Field()

