# -*- coding: utf-8 -*-
from ftfy import fix_text


class CleanTextPipeline(object):
    def process_item(self, item, spider):
        item["title"] = fix_text(item["title"])
        item["text"] = fix_text(item["text"])
        item["description"] = fix_text(item["description"])
        item["subtopic"] = fix_text(item["subtopic"])
        return item
