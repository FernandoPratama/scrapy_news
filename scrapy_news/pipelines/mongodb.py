# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from scrapy.exceptions import CloseSpider

from pymongo.errors import OperationFailure
import logging

class MongoDBPipeline(object):
    def __init__(self, mongo_uri, mongo_db, mongo_collection):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.mongo_collection = mongo_collection
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.screen_logger = logging.getLogger('mongo logger')
        self.screen_logger.setLevel(logging.INFO)
        if not self.screen_logger.handlers:
            handler = logging.StreamHandler()
            handler.setLevel(logging.INFO)
            handler.setFormatter(formatter)
            self.screen_logger.addHandler(handler)

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGODB_URI'),
            mongo_db=crawler.settings.get('MONGODB_DB'),
            mongo_collection=crawler.settings.get('MONGODB_COLLECTION')
        )

    def open_spider(self, spider):
        try:
            self.client = pymongo.MongoClient(self.mongo_uri)
            self.client.server_info()
            self.collection = self.client[self.mongo_db][self.mongo_collection]
        except OperationFailure as e:
            self.screen_logger.fatal(e)
            self.screen_logger.fatal("Check MONGO_URI in settings.py")
            raise CloseSpider("Authentication failed in MongoDBPipeline")

    def close_spider(self, spider):
        self.client.close()


    def process_item(self, item, spider):
        self.collection.replace_one({"doc_id":item['doc_id'],
                                     "domain":item['domain']},
                                    item,
                                    upsert=True)
        return item
