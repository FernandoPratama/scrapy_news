from scrapy.exceptions import DropItem
from ftfy import fix_text
import logging


class ValidatePipeline(object):
    def __init__(self, log_every):
        formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
        screen_logger = logging.getLogger("screen logger")
        screen_logger.setLevel(logging.INFO)
        if not screen_logger.handlers:
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(logging.INFO)
            stream_handler.setFormatter(formatter)
            screen_logger.addHandler(stream_handler)

        self.screen_logger = screen_logger
        drop_logger = logging.getLogger("drop logger")
        drop_logger.setLevel(logging.INFO)
        if not drop_logger.handlers:
            file_handler = logging.FileHandler("log.txt")
            file_handler.setFormatter(formatter)
            drop_logger.addHandler(file_handler)
        self.drop_logger = drop_logger

        self.count = 0
        self.count_drop = 0
        self.log_every = log_every

    @classmethod
    def from_crawler(cls, crawler):
        return cls(log_every=crawler.settings.get("LOG_EVERY"))

    def process_item(self, item, spider):
        required_fields = ["doc_id", "title", "text", "description",
                           "published_timestamp", "published_date",
                           "crawled_timestamp", "crawled_date", "url", "topic",
                           "language", "source_type", "source_name", "domain"]

        for required_field in required_fields:
            if not item[required_field]:
                self.count_drop = self.count_drop + 1
                drop_message = "Url: %s missing: %s" % (item["url"],
                                                        required_field)
                self.drop_logger.info(drop_message)
                raise DropItem("Missing %s field" % required_field)

        self.count += 1
        # if self.count % spider.log_every == 0:
        if self.count % self.log_every == 0:
            success_message = \
                "%d items from %s added to database, %d items dropped" \
                % (self.count, item["domain"], self.count_drop)
            self.screen_logger.info(success_message)
        return item
