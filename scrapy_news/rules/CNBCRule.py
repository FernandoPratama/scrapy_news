import datetime
from scrapy_news.rules.rules import ArticleRules
import re
import pytz


class CNBCRule(ArticleRules):
    def __init__(self, response):
        super().__init__(response)
        self.source_type = "news"
        self.source_name = "cnbc"
        self.domain = "cnbc.com"
        self.source_country = "USA"

    def parse_id(self):
        doc_id = self.response.xpath(
            '//meta[@property="pageNodeId"]/@content').extract_first()
        return doc_id

    def parse_title(self):
        title = self.response.xpath(
            '//meta[@property="og:title"]/@content').extract_first()
        title = re.sub(r'&#\d;', '', title)
        return title

    def parse_description(self):
        description = self.response.xpath(
            '//meta[@name="description"]/@content').extract_first()
        return description

    def parse_url(self):
        return self.response.url

    def parse_tags(self):
        tags = self.response.xpath(
            '//meta[@property="article:tag"]/@content').extract()
        return tags
   
    def parse_language(self):
        language = self.response.xpath(
            '//meta[@itemprop="inLanguage"]/@content').extract_first()
        return language

    def parse_image(self):
        image = self.response.xpath(
            '//meta[@property="og:image"]/@content').extract_first()
        return image

    def parse_text(self):
        try:
            text = self.response.xpath(
                '//div[@itemprop="articleBody"]//p/text()').extract()
            text = '\n'.join([t.strip() for t in text])
        except:
            return ""
        else:
            return text

    def parse_raw_subtopic(self):
        try:
            raw_subtopic = self.response.xpath(
                '//meta[@property="article:tag"]/@content')[1].extract()
            raw_subtopic = raw_subtopic.lower()
            raw_subtopic = re.sub(r'&#\d;|&.*;', '', raw_subtopic)
        except:
            return ""
        else:
            return raw_subtopic

    def parse_raw_topic(self):
        try:
            raw_topic = self.response.xpath(
                '//meta[@property="article:section"]/@content').extract_first()
            raw_topic = raw_topic.lower()
        except:
            return ""
        else:
            return raw_topic

    def parse_pdate(self):
        try:
            pdate = self.response.xpath(
                '//meta[@property="article:published_time"]/@content').extract_first()
            pdate = datetime.datetime.strptime(pdate, '%Y-%m-%dT%H:%M:%S%z')
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
        except:
            return None
        else:
            return pdate
