from dateutil.parser import parse
from scrapy_news.rules.rules import ArticleRules
import pytz


class GuardianRule(ArticleRules):
    def __init__(self,response):
        super().__init__(response)
        self.source_type = "news"
        self.source_name = "guardian"
        self.domain = "theguardian.com"
        self.source_country = "GBR"
   
    def parse_id(self):
        doc_id = self.response.xpath(
            '//meta[@property="og:url"]/@content').extract_first()
        return doc_id

    def parse_title(self):
        title = self.response.xpath(
            '//meta[@property="og:title"]/@content').extract_first()
        return title

    def parse_description(self):
        description = self.response.xpath(
            '//meta[@property="og:description"]/@content').extract_first()
        return description

    def parse_url(self):
        return self.response.url

    def parse_tags(self):
        try:
            tags = self.response.xpath(
                '//meta[@property="article:tag"]/@content').extract_first()
            tags = tags.split(",")
        except:
            return []
        else:
            return tags

    def parse_language(self):
        return "en-UK"

    def parse_image(self):
        image = self.response.xpath(
            '//img[@class="maxed responsive-img"]/@src').extract_first()
        if not image:
            return ""
        else:
            return image

    def parse_text(self):
        try:
            text = self.response.xpath(
                '//div[@class="content__article-body from-content-api js-article__body"]//p/text()').extract()
            text='\n'.join([t.strip() for t in text])
        except:
            return ""
        else:
            return text

    def parse_raw_subtopic(self):
        try:
            raw_subtopic = self.response.xpath(
                '//li[@class="signposting__item signposting__item--current"]//a//text()').extract_first()
            raw_subtopic = raw_subtopic.lower()
        except:
            return ""
        return raw_subtopic

    def parse_raw_topic(self):
        try:
            raw_topic = self.response.xpath(
                '//li[@class="signposting__item signposting__item--parent"]//a//text()').extract_first()
            raw_topic = raw_topic.lower()
            if raw_topic == "sport":
                raw_topic = "sports"
            elif raw_topic == "tech":
                raw_topic = "technology"
        except:
            return ""
        else:
            return raw_topic
                
    def parse_pdate(self):
        try:
            pdate = self.response.xpath(
                '//meta[@property="article:published_time"]/@content').extract_first()
            pdate=parse(pdate)
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
        except:
            return None
        else:
            return pdate
