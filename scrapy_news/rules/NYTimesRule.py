import pytz
from dateutil.parser import parse
from scrapy_news.rules.rules import ArticleRules


class NYTimesRule(ArticleRules):
    def __init__(self, response):
        super().__init__(response)
        self.source_type = "news"
        self.source_name = "nytimes"
        self.domain = "nytimes.com"
        self.source_country = "USA"

    def parse_id(self):
        doc_id = self.response.xpath(
            '//meta[@name="articleid"]/@content').extract_first()
        return doc_id

    def parse_title(self):
        title = self.response.xpath(
            '//meta[@property="og:title"]/@content').extract_first()
        return title

    def parse_description(self):
        description = self.response.xpath(
            '//meta[@name="description"]/@content').extract_first()
        return description

    def parse_url(self):
        return self.response.url

    def parse_tags(self):
        try:
            tags = self.response.xpath(
                '//meta[@name="keywords"]/@content').extract_first()
            tags = tags.split(",")
        except:
            return []
        else:
            return tags

    def parse_language(self):
        language = self.response.xpath(
            '//meta[@itemprop="inLanguage"]/@content').extract_first()
        return language

    def parse_image(self):
        image = self.response.xpath(
            '//meta[@property="og:image"]/@content').extract_first()
        if (not image
            or image ==
            "https://static01.nyt.com/images/icons/t_logo_291_black.png"
            or image ==
            "https://static01.nyt.com/images/common/icons/t_wb_75.gif"):
            return ""
        else:
            return image

    def parse_text(self):
        try:
            text = self.response.xpath(
                '//p[@class="story-body-text story-content"]//text()').extract()
            if not text:
                text = self.response.xpath(
                    '//div[@class="articleBody"]/p//text()').extract()
            text = '\n'.join([t.strip() for t in text])
            text = text.replace("&mdash", "")
        except:
            return ""
        else:
            return text

    def parse_raw_subtopic(self):
        try:
            raw_subtopic = self.response.xpath(
                '//meta[@itemprop="articleSection"]/@content').extract_first()
            raw_subtopic = raw_subtopic.lower()
        except:
            return ""
        else:
            return raw_subtopic

    def parse_raw_topic(self):
        try:
            raw_topic = (self.response.xpath(
                '//meta[@property="article:top-level-section"]/@content')
                         .extract_first())
            raw_topic = raw_topic.lower()
        except:
            return ""
        else:
            return raw_topic
                
    def parse_pdate(self):
        try:
            pdate = self.response.xpath(
                '//meta[@itemprop="datePublished"]/@content').extract_first()
            pdate = parse(pdate)

            # The old date does not have time
            if not pdate.tzinfo:
                pdate = pytz.timezone("America/New_York").localize(pdate)
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
        except:
            return None
        else:
            return pdate
