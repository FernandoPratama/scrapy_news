import datetime
from scrapy_news.rules.rules import ArticleRules
import pytz
import re
from ..utility import split_camel


class ReutersRule(ArticleRules):
    def __init__(self,response):
        super().__init__(response)
        self.source_type = "news"
        self.source_name = "reuters"
        self.domain = "reuters.com"
        self.source_country = "USA"

    def parse_id(self):
        try:
            # doc id is always after 'id' on the last part of url
            doc_id = re.match(r".*id(.*)$", self.response.url).group(1)
        except:
            return ""
        else:
            return doc_id

    def parse_title(self):
        title = self.response.xpath(
            '//meta[@property="og:title"]/@content').extract_first()
        return title

    def parse_description(self):
        description = self.response.xpath(
            '//meta[@name="description"]/@content').extract_first()
        return description

    def parse_url(self):
        return self.response.url

    def parse_tags(self):
        try:
            tags = self.response.xpath(
                '//meta[@property="og:article:tag"]/@content').extract_first()
            tags = tags.split(",")
        except:
            return []
        else:
            return tags

    def parse_language(self):
        language = self.response.xpath(
            '//meta[@property="og:locale"]/@content').extract_first()
        return language

    def parse_image(self):
        image = self.response.xpath(
            '//link[@rel="image_src"]/@href').extract_first()
        if (not image or image ==
            "https://s4.reutersmedia.net/resources_v2/images/rcom-default.png"):
            image = ""
        return image

    def parse_text(self):
        try:
            text = self.response.xpath(
                    '//div[@class="ArticleBody_body_2ECha"]/p/text()').extract()
            if not text:
                text = self.response.xpath(
                    '//div[@class="ArticleBody_body_2ECha"]/pre/text()').extract()
            # Trim extra whitespace and join by newline
            text = '\n'.join([t.strip() for t in text])
        except:
            return ""
        else:
            return text

    def parse_raw_subtopic(self):
        try:
            raw_subtopic = self.response.xpath(
                '//meta[@name="DCSext.ContentChannel"]/@content').extract_first()
            if raw_subtopic == 'ousivMolt':
                raw_subtopic = 'business'
            raw_subtopic = split_camel(raw_subtopic).lower()
        except:
            return ""
        else:
            return raw_subtopic

    def parse_raw_topic(self):
        try:
            raw_topic = self.response.xpath(
                '//meta[@property="og:article:section"]/@content').extract_first()
            if not raw_topic:
                raw_topic = self.response.xpath(
                    '//span[@class="article-section"]/a/text()').extract_first()
            raw_topic = split_camel(raw_topic).lower()
        except:
            return ""
        else:
            return raw_topic
                
    def parse_pdate(self):
        try:
            pdate = self.response.xpath(
                '//meta[@name="REVISION_DATE"]/@content').extract_first()
            pdate = datetime.datetime.strptime(pdate, '%a %b %d %H:%M:%S %Z %Y')
            tz = pytz.timezone("UTC")
            pdate = tz.localize(pdate)
        except:
            return None
        else:
            return pdate
