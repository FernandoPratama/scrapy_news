from scrapy_news.rules.rules import ArticleRules
import datetime
import re
import json
import pytz


class TimeRule(ArticleRules):
    def __init__(self, response):
        super().__init__(response)
        self.source_type = "news"
        self.source_name = "time"
        self.domain = "time.com"
        self.source_country = "USA"
        self.data = self.parse_content()

    def parse_content(self):
        try:
            content = self.response.xpath(
                '//script[@type="application/ld+json"]/text()').extract_first()
            data = json.loads(content, encoding='utf-8')
        except:
            return dict()
        else:
            return data

    def parse_id(self):
        return self.response.url

    def parse_title(self):
        title = self.response.xpath(
            '//meta[@property="og:title"]/@content').extract_first()
        return title

    def parse_description(self):
        description = self.response.xpath(
            '//meta[@name="description"]/@content').extract_first()
        return description

    def parse_url(self):
        return self.response.url

    def parse_tags(self):
        try:
            tags = self.response.xpath(
                '//meta[@name="keywords"]/@content').extract_first()
            tags = tags.split(", ")
        except:
            return []
        else:
            return tags

    def parse_language(self):
        language = self.response.xpath(
            '//meta[@property="og:locale"]/@content').extract_first()
        return language

    def parse_image(self):
        image = self.response.xpath(
            '//div[@class="row"]/picture/img/@src').extract_first()
        if not image:
            return ""
        return image

    def parse_text(self):
        try:
            text = self.data.get('articleBody', "")
            text = text.replace('You are getting a free preview of a TIME Magazine article from our archive. Many of our articles are reserved for subscribers only. Want access to more subscriber-only content, click here to subscribe. ','')
            text = re.sub(r'\&\#\d{4}', '', text)
        except:
            return ""
        else:
            return text

    def parse_raw_subtopic(self):
        try:
            raw_subtopic = self.response.xpath(
                '//div[@class="row"]/a[@class="text size-1x-small font-accent color-brand all-caps"]/text()').extract_first()
            raw_subtopic = raw_subtopic.lower()
        except:
            return ""
        else:
            return raw_subtopic

    def parse_raw_topic(self):
        try:
            raw_topic = self.data.get('articleSection', "")
            raw_topic = raw_topic.lower()
        except:
            return ""
        else:
            return raw_topic

    def parse_pdate(self):
        try:
            pdate = self.data.get('datePublished')
            pdate = ''.join([pdate, 'GMT'])
            pdate = datetime.datetime.strptime(pdate, '%Y-%m-%dT%H:%M:%S%Z')
            tz = pytz.timezone("UTC")
            pdate = tz.localize(pdate)
        except:
            return None
        else:
            return pdate
