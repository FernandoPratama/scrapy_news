from abc import ABCMeta, abstractmethod

class ArticleRules(metaclass=ABCMeta):
    T = ["economy", "finance", "social", "legal", "governance", "market",
         "business", "politics", "technology", "sports", "health", "world",
         "environment"]

    @abstractmethod
    def __init__(self,response):
        self.response = response

    @abstractmethod
    def parse_id(self):
        pass

    @abstractmethod
    def parse_title(self):
        pass

    @abstractmethod
    def parse_description(self):
        pass

    @abstractmethod
    def parse_url(self):
        pass

    @abstractmethod
    def parse_tags(self):
        pass

    @abstractmethod
    def parse_language(self):
        pass

    @abstractmethod
    def parse_image(self):
        pass

    @abstractmethod
    def parse_text(self):
        pass

    @abstractmethod
    def parse_raw_subtopic(self):
        pass

    @abstractmethod
    def parse_raw_topic(self):
        pass

    def parse_topic_subtopic(self):
        raw_topic = self.parse_raw_topic()
        raw_subtopic = self.parse_raw_subtopic()
        topic = ""
        subtopic = ""
        for t in self.T:
            if t in raw_topic:
                topic = t
                subtopic = raw_subtopic
                break
            elif t in raw_subtopic:
                topic = t
                subtopic = raw_topic
                break
        if not topic:
            topic = "others"
            subtopic = raw_subtopic
        return topic, subtopic

    @abstractmethod           
    def parse_pdate(self):
        pass

    def parse_pdate_ptimestamp(self):
        pub_date = self.parse_pdate()
        if pub_date:
            pub_timestamp = int(pub_date.timestamp())
            return pub_date, pub_timestamp
        else:
            return None, None
