import re
from scrapy.http import Request
from scrapy_news.items import NewsItem
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import datetime
from scrapy_news.rules.CNBCRule import CNBCRule
from scrapy_news.utility import parse_all


class CNBCNewSpider(CrawlSpider):
    """Spider for scraping CNBC.com"""
    name = "cnbc"
    allowed_domains = ["cnbc.com"]
    start_urls = ["http://www.cnbc.com/site-map/"]
    dlinks = (
        '//www.cnbc.com/live-tv/',
        '//www.cnbc.com/latest-video/',
        '//www.cnbc.com/business-day/',
        '//www.cnbcprime.com/',
        '//www.cnbc.com/video-ceo-interviews/',
        '//www.cnbc.com/video-analyst-interviews/',
        '//www.cnbc.com/quotes/',
        '//www.cnbc.com/markets-europe/',
        '//www.cnbc.com/us-markets/',
        '//www.cnbc.com/pre-markets/',
        '//www.cnbc.com/stocks/',
        '//www.cnbc.com/bonds/',
        '//www.cnbc.com/commodities/',
        '//www.cnbc.com/currencies/',
        '//www.cnbc.com/exchange-traded-funds/',
        '//www.cnbc.com/mutual-funds/',
        '//www.cnbc.com/world/',
        '//www.cnbc.com/markets/')

    rules = [Rule(LinkExtractor(allow=r"//www.cnbc.com/.*",
                                deny=dlinks,
                                restrict_xpaths='//div[@class="story"]'),
                  callback="request_page",
                  follow=False)]

    def __init__(self, **kw):
        super(CNBCNewSpider, self).__init__(**kw)

    @staticmethod
    def parse_article(response):
        """Function to scrap each source page"""
        item = NewsItem()
        ogtype = response.xpath(
            '//meta[@property="og:type"]/@content').extract_first()
        if ogtype == "video":
            pass
        else:
            rule = CNBCRule(response)
            try:
                item = parse_all(rule)
            except Exception as e:
                print(e)
                print(response.url)

            cdate = datetime.datetime.utcnow()
            item['crawled_date'] = cdate
            item['crawled_timestamp'] = int(cdate.timestamp())
        return item

    def request_page(self, response):
        """
        Given the topic from sitemap, request all pages within the topic
        :param response: Response from the first page of the topic
        :return: Generate request of each article on every page
        """
        last_page = response.xpath(
            '//a[@class="lastLink"]/@href').extract_first()
        if last_page:
            page_name = re.match('(.*page=)?([0-9]+)$', last_page).group(1)
            page_number = int(
                re.match('(.*page=)?([0-9]+)$', last_page).group(2))
            all_pages = ["http://www.cnbc.com" + page_name + str(p)
                         for p in range(1, page_number + 1)]
            for page in all_pages:
                yield Request(page, callback=self.request_article)

    def request_article(self, response):
        """
        Given a page of cnbc topic, request all article links and parse them
        :param response: Response from a page of the topic
        :return: Generate news item of each article
        """
        items = response.xpath(
            '//ul[@class="stories_assetlist"]//a/@href').extract()
        for item in items:
            i = ''.join(["http://www.cnbc.com", item])
            yield Request(i, callback=self.parse_article)
