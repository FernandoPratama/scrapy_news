# -*- coding: utf-8 -*-
from scrapy.http import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import datetime

from scrapy_news.items import NewsItem
from scrapy_news.rules.GuardianRule import GuardianRule
from scrapy_news.utility import parse_all


class GuardianNewSpider(CrawlSpider):
    """Spider for scraping theguardian.com"""
    name = "guardian"
    allowed_domains = ["theguardian.com"]
    start_urls = ["https://www.theguardian.com/index/subjects"]
    rules = [Rule(LinkExtractor(allow=r"https://www.theguardian.com/index/subjects/.*",
                                restrict_xpaths='//li[@class="linkslist__item"]'),
                  callback="request_part",
                  follow=False)]

    def __init__(self, **kw):
        super(GuardianNewSpider, self).__init__(**kw)

    @staticmethod
    def parse_article(response):
        """Function to scrap each source page"""
        rule = GuardianRule(response)
        item = NewsItem()
        try:
            item = parse_all(rule)
        except Exception as e:
            print(e)
            print(response.url)

        cdate = datetime.datetime.utcnow()
        item['crawled_date'] = cdate
        item['crawled_timestamp'] = int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function to request each aricle page"""
        items = response.xpath(
            '//div[@class="fc-item__container"]//a/@href').extract()
        for item in items:
            yield Request(item, callback=self.parse_article)

        current = int(response.xpath(
            '//span[@aria-label="Current page"]/text()').extract_first())

        if current:
            next_page = current + 1
            next_page = str(next_page)
            query = ''.join(
                ['//div[@class="pagination__list"]//a[@data-page="',
                 next_page, '"]/@href'])
            url = response.xpath(query).extract_first()
            if url:
                yield Request(url, callback=self.request_article)

    def request_part(self, response):
        """Function to scrap each subject page that contains links of articles"""
        links = response.xpath('//li[@class="linkslist__item"]/a/@href').extract()
        for link in links:
            yield Request(link, callback=self.request_article)
