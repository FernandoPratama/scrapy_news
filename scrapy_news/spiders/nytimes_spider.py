from scrapy.http import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import datetime

from scrapy_news.items import NewsItem
from scrapy_news.rules.NYTimesRule import NYTimesRule
from scrapy_news.utility import parse_all


class NYtimesNewSpider(CrawlSpider):
    """Spider for scraping nytimes.com"""
    name = "nytimes"
    allowed_domains = ["nytimes.com"]
    start_urls = ["http://spiderbites.nytimes.com/"]
    rules = [Rule(LinkExtractor(allow=r'free_198[7-9]/index.html|free_199[0-9]/index.html|free_20\d{2}/index.html',
                                restrict_xpaths='//div[@id="articles_free"]'),
                  callback="request_part",
                  follow=False)]

    def __init__(self, **kw):
        super(NYtimesNewSpider, self).__init__(**kw)

    @staticmethod
    def parse_article(response):
        """Function to scrap each source page"""
        rule = NYTimesRule(response)
        item = NewsItem()
        try:
            item = parse_all(rule)
        except Exception as e:
            print(e)
            print(response.url)

        cdate = datetime.datetime.utcnow()
        item['crawled_date'] = cdate
        item['crawled_timestamp'] = int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function to request each aricle page"""
        items = response.xpath('//div[@id="mainContent"]//a/@href').extract()
        for item in items:
            yield Request(item, callback=self.parse_article)

    def request_part(self, response):
        """Function to scrap part page that contains links of articles"""
        links = response.xpath(
            '//div[@class="articlesMonth"]//a/@href').extract()
        for link in links:
            link = ''.join(["http://spiderbites.nytimes.com", link])
            yield Request(link, callback=self.request_article)
