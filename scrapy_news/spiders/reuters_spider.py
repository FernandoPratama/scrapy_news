from scrapy.http import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import datetime

from scrapy_news.items import NewsItem
from scrapy_news.rules.ReutersRule import ReutersRule
from scrapy_news.utility import parse_all


class ReutersNewsSpider(CrawlSpider):
    """Spider for scraping reuters.com"""
    name = "reuters"
    allowed_domains = ["www.reuters.com"]
    start_urls = ["http://www.reuters.com/resources/archive/us"]
    rules = [
        Rule(LinkExtractor(allow=r'resources/archive/us/\d{4}.html',
                           restrict_xpaths='//div[@class="moduleBody"]/h4'),
             callback="request_part", follow=False),
        Rule(LinkExtractor(allow=r'resources/archive/us/\d{8}.html'),
             callback="request_article", follow=False)]

    def __init__(self, **kw):
        super(ReutersNewsSpider, self).__init__(**kw)

    @staticmethod
    def parse_article(response):
        """Function to scrap each source page"""
        rule = ReutersRule(response)
        item = NewsItem()
        try:
            item = parse_all(rule)
        except Exception as e:
            print(e)
            print(response.url)

        cdate = datetime.datetime.utcnow()
        item['crawled_date'] = cdate
        item['crawled_timestamp'] = int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function to request each source page"""
        items = response.xpath(
            '//div[@class="module"]/div[@class="headlineMed"]/a/@href').extract()
        for item in items:
            yield Request(item, callback=self.parse_article)

    def request_part(self, response):
        """Function to request each page that contains links of article pages"""
        links = response.xpath('//div[@class="moduleBody"]/p/a/@href').extract()
        for link in links:
            l = ''.join(["http://www.reuters.com", link])
            yield Request(l, callback=self.request_article)
