from scrapy.http import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import datetime

from scrapy_news.items import NewsItem
from scrapy_news.rules.TimeRule import TimeRule
from scrapy_news.utility import parse_all


class TimeNewSpider(CrawlSpider):
    """Spider for scraping time.com"""
    name = "time"
    allowed_domains = ["time.com"]
    start_urls = ["http://time.com/html-sitemap/"]
    rules = [Rule(LinkExtractor(allow=r'http://time.com/html-sitemap/time-section-[a-z]*/',
                                restrict_xpaths = '//div[@class="ti-sitemap-list clearfix"]'),
                  callback="request_part",
                  follow=False)]

    def __init__(self, **kw):
        super(TimeNewSpider, self).__init__(**kw)

    @staticmethod
    def parse_article(response):
        """Function to scrap each source page"""
        rule = TimeRule(response)
        item = NewsItem()
        try:
            item = parse_all(rule)
        except Exception as e:
            print(e)
            print(response.url)

        cdate = datetime.datetime.utcnow()
        item['crawled_date'] = cdate
        item['crawled_timestamp'] = int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function request each article page"""
        items = response.xpath(
            '//div[@class="ti-sitemap-list clearfix"]/ul/li/a/@href').extract()
        for item in items:
            yield Request(item, callback=self.parse_article)

    def request_part(self, response):
        """Function request each part page that contains article links"""
        links = response.xpath(
            '//div[@class="ti-sitemap-list clearfix"]/ul/li/a/@href').extract()
        for link in links:
            yield Request(link, callback=self.request_article)
