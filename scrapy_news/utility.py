import re
import string

from scrapy_news.items import NewsItem


def split_camel(text):
    """
    Split camel string by space, if text contains non alphanumeric characters
    then return original text
    e.g.:
      politicsNews -> politics  News
      HTMLTag -> HTML tag
      basicMaterialSector -> basic Material Sector
      top1News -> top1News
      GCA-Commodities -> GCA-Commodities
    :param text: String written in camel format
    :return: Input text separated by space
    """
    if any(t.isdigit() or t in string.punctuation for t in text):
        return text
    else:
        return re.sub(r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r' \1', text)


def parse_all(article_rule):
    news_item = NewsItem()
    news_item['source_type'] = article_rule.source_type
    news_item['source_name'] = article_rule.source_name
    news_item['domain'] = article_rule.domain
    news_item['source_country'] = article_rule.source_country
    news_item['text'] = article_rule.parse_text()
    news_item['doc_id'] = article_rule.parse_id()
    news_item['title'] = article_rule.parse_title()
    news_item['description'] = article_rule.parse_description()
    news_item['url'] = article_rule.parse_url()
    news_item['tags'] = article_rule.parse_tags()
    news_item['language'] = article_rule.parse_language()
    news_item['image'] = article_rule.parse_image()
    news_item['topic'], news_item['subtopic'] = \
        article_rule.parse_topic_subtopic()
    news_item['published_date'], news_item['published_timestamp'] = \
        article_rule.parse_pdate_ptimestamp()
    return news_item
